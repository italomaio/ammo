import axios from 'axios'

var searchAPI = axios.create({
  baseURL: 'http://127.0.0.1:9999/api/'
})

export default {
  search: (qry) => {
    return searchAPI(`products/search?query=${qry || ""}`);
  }
}
