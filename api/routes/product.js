import express from 'express';

import Product from '../controllers/product'

var Router = express.Router()

Router.route('/products/fill')

.post((req, res, next) => {
  Product.fill(req, res, next).then(resp => {
    res.status(200).json(resp);
  }).catch(err => {
    res.send(err);
  })
})

Router.route('/products/search')

.get((req, res, next) => {
  Product.search(req, res, next).then(resp => {
    res.status(200).json(resp);
  }).catch(err => {
    res.send(err);
  })
})

export default Router
