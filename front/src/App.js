import React, { Component } from 'react';
import { connect } from 'react-redux';
import logo from './logo_mm.svg';
import './App.css';

import Search from './components/Search/Search';
import Pagination from './components/Pagination/Pagination';

import CurrencyFormat from 'react-currency-format';

import API from './services/Search';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
        pageOfItems: []
    };

    this.onChangePage = this.onChangePage.bind(this);
  }

  componentDidMount() {
    API.search().then(resp => {
      this.props.dispatch({ type: 'FETCH_PRODUCTS', payload: resp.data });
    });
  }

  handleSelectView = (e) => {
    this.props.dispatch({ type: 'CHANGE_PAGE_SIZE', pageSize: e.target.value });
  }

  onChangePage(pageOfItems) {
      this.setState({ pageOfItems: pageOfItems });
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <Search />
        </header>
        <section className="Search-term">
          <h3>{ this.props.searchTerm ? this.props.searchTerm : 'Lista de produtos' }</h3>
        </section>
        <div className="search-results">
          <div className="search-results__result-count">
            <div className="search-results__result-count__text"><strong>{this.props.products.length}</strong> produtos encontrados</div>
          </div>
          <div className="search-results__result-list">
            {this.state.pageOfItems.map(item =>
              <div key={item._id} className="search-results__result-list__item">
                <div className="product-result">
                  <div className="product-result__thumbnails">
                    {item.images.map((image, i) =>
                      <img key={i} src={image[0]} />
                    )}
                  </div>
                  <div className="product-result__details">
                    <div className="product-result__details__info">
                      <div className="top-info">{item.name}</div>
                      <div className="bottom-info">
                        <span>{item.brand}</span>
                        <span>&nbsp;·&nbsp;</span>
                        <span>{item.model}</span>
                        <span>&nbsp;·&nbsp;</span>
                        <span>{item.color}</span>
                      </div>
                    </div>
                    <div className="product-result__details__price">
                      <CurrencyFormat className="product-result__details__price--crossed" value={item.price_old} displayType={'text'} thousandSeparator={"."} decimalSeparator={","} decimalScale={2} fixedDecimalScale={true} prefix={'R$'} />
                      &nbsp;por&nbsp;
                      <CurrencyFormat value={item.price_new} displayType={'text'} thousandSeparator={"."} decimalSeparator={","} decimalScale={2} fixedDecimalScale={true} prefix={'R$'} />
                    </div>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
        <div className="container">
          <div className="">
            <div className="select-wrapper">
              <select className="select-box" onChange={this.handleSelectView}>
                <option value="1">1 produto por página</option>
                <option value="5">5 produtos por página</option>
                <option value="10">10 produtos por página</option>
                <option value="20">20 produtos por página</option>
              </select>
            </div>
          </div>
          <div className="">
            <Pagination pageSize={this.props.pageSize} items={this.props.products} onChangePage={this.onChangePage} />
          </div>
        </div>
        <br /><br />
      </div>
    );
  }
}

const MapStateToProps = store => {
  return {
    products: store.searchState.products,
    pageSize: store.searchState.pageSize,
    searchTerm: store.searchState.searchTerm
  }
};

export default connect(MapStateToProps)(App);
