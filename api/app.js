import express    from 'express';
import bodyParser from 'body-parser';
import path       from 'path';
import cors       from 'cors';
import expressValidator from 'express-validator';
import Routes     from './routes';

var app = express();

app.use(expressValidator());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(cors({ credentials: true, origin: true }));

var port = process.env.port || 9999;

app.use("/", Routes);

app.listen(port);
