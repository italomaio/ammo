import React, { Component } from 'react';
import { connect } from 'react-redux';

import './Search.css';

import API from '../../services/Search';

class Search extends Component {

  constructor(props) {
    super(props);
    this.state = {
      search: ''
    }
  }

  clearInput = (e) => {
      this.setState({
        search: ''
      });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    API.search(this.state.search)
       .then(resp => {
         console.log(resp.data)
         this.props.dispatch({ type: 'FETCH_PRODUCTS', payload: resp.data })
         this.props.dispatch({ type: 'CHANGE_SEARCH_TERM', payload: { searchTerm: this.state.search } })
       })
       .catch(err => console.log(err))
  }

  handleOnChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  render() {
    return (
      <div className="Search">
        <form onSubmit={this.handleSubmit}>
          <input type="text" name="search" onChange={this.handleOnChange} value={this.state.search} />
          <span style={{ display: this.state.search === '' ? 'none' : 'block' }} onClick={this.clearInput}>✖︎</span>
        </form>
      </div>
    );
  }
}

export default connect()(Search);
