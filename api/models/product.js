import mongoose, { Schema } from 'mongoose'

var Product = new Schema({
  name: Schema.Types.String,
  category: Schema.Types.String,
  images: Schema.Types.Array,
  brand: Schema.Types.String,
  model: Schema.Types.String,
  color: Schema.Types.String,
  price_old: Schema.Types.Number,
  price_new: Schema.Types.Number
});

export default mongoose.model('produtos', Product)
