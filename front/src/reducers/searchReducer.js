const initialState = {
  products: [],
  pageSize: 1,
  searchTerm: ''
};

export const searchReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'FETCH_PRODUCTS':
        return {
          ...state,
          products: action.payload
        }
      break;
    case 'CHANGE_PAGE_SIZE':
        return {
          ...state,
          pageSize: parseInt(action.pageSize)
        }
      break;
    case 'CHANGE_SEARCH_TERM':
        return {
          ...state,
          searchTerm: action.payload.searchTerm
        }
      break;
    default:
        return state;
  }
}
