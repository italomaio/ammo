import express from 'express';
import Product from './product';

var Router = express.Router();

Router.use("/api/", Product)

export default Router
