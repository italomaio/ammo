import mongoose from 'mongoose';

const connection_string = 'mongodb://ammo:senhaammo123@ds227373.mlab.com:27373/ammo';

class Database {

  constructor(props) {
    mongoose.Promise = global.Promise
    this._connect()
  }

  _connect() {
    mongoose.connect(connection_string, (err, conn) => {
      if (err) throw err;
      this.connection = conn;

      this.connection.on('connected', function (e) {
          console.log(e, "logou");
      });

      return this.connection;
    })
  }

  static close() {
    this.connection.close()
    console.log("closed conn");
  }

}

export default new Database()
