import db from '../database';
import Product from '../models/product'

export default {
  fill: (req, res, next) => {
    return new Promise((resolve, reject) => {

      let product = req.body;

      if (product == null || product === 'undefined' || !product) {
        reject();
      }

      Product.create({
        name: product.name,
        category: product.category,
        brand: product.brand,
        model: product.model,
        color: product.color,
        price_old: product.price_old,
        price_new: product.price_new,
        images: [
          'https://placehold.it/80x80',
          'https://placehold.it/80x80',
          'https://placehold.it/80x80',
          'https://placehold.it/80x80'
        ],
      }).then(resp => {

        resolve(db.connection.collection("produtos").find({}).toArray());

      });

    });
  },
  search: (req, res, next) => {
    return new Promise((resolve, reject) => {

      let { query: qry }    = req.query;
      let filter = !qry == 'undefined' || !qry || qry == '' ? {} : { name: { $regex: '.*' + qry + '.*' } };

      console.log(filter);
      Product.find(filter)
        .then(resp => {
          resolve(resp)
        })
        .catch(err => reject(err))

    })
  }
}
