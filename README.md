# Teste técnico para AMMO Varejo

Desenvolvido teste técnico para comprovar conhecimentos relacionados a **stack** utilizada pela empresa **AMMO Varejo**

#### Tecnologias utilizadas

* Node.JS
* React / Redux
* ES6
* MongoDB

## Intruções para Build local

Primeiramente deve-se fazer o clone deste repositório. Logo em seguida, instalar as dependencias tanto para o projeto **API** quanto o **FRONT**;

> cd AMMO/front && yarn install

> cd AMMO/api && yarn install

Com as dependencias instaladas corretamente podemos iniciar os serviços:

### Iniciando serviço de Back-end (API)

> npm install -g nodemon

> nodemon AMMO/api/start.js 

### Iniciando serviço de Front-end

> npm install -g node-static

> cd AMMO/front/build/

> static

> Acessar http://localhost:8080/